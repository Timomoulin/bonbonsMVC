<?php
$action = $_GET["action"];


switch ($action) {
    case "formulaire":
        include("vues/formAdmin.php");
        break;

        //Fair un case de traitement du formulaire
        //Qui utilise la fonction statique verification de Admin
        //pour verifier si le login et mdp corespond a une ligne dans la table

        //hash("sha256", "admin");
    case "verif":

        $login = securiser($_POST["login"]);
        $mdp = securiser($_POST["mdp"]);

        $rep = Admin::verifier($login, $mdp);

        if ($rep == true) {
            $_SESSION["autorisation"] = "OK";
            if (isset($_SESSION["error"])) {
                unset($_SESSION["error"]);
            }
            $lesProduits = Produit::afficherTous();
            header("location:index.php?uc=admin&action=espace");
        } else {
            $_SESSION["error"] = "Echec connexion";

            header("location:index.php?uc=admin&action=formulaire");
        }

        break;

    case "deco":
        Admin::deco();
        include("vues/accueil.php");
        break;

    case "espace":
        //Si $_SESSION["autorisation"] $autorisation prend sa valeur 
        //Sinon $autorisation devient faux
        $autorisation = $_SESSION["autorisation"] ?? false;
        if ($autorisation) {
            $lesProduits = Produit::afficherTous();
            include("vues/accueilAdmin.php");
        } else {
            include("vues/formAdmin.php");
        }
        // if (isset($_SESSION["autorisation"]) && $_SESSION["autorisation"] === "OK") {
        //     include("vues/accueilAdmin.php");
        // }

        break;
}
