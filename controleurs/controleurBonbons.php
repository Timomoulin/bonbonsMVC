<?php
$action = $_GET["action"];

switch ($action) {
    case "liste":
        $lesProduits = Produit::afficherTous();
        include("vues/listeProduits.php");
        break;
    case "recherche":
        $nomRechercher = securiser($_POST["recherche"]);
        $nomRechercher = mb_strtolower($nomRechercher);
        $lesProduits = Produit::recherche($nomRechercher);
        include("vues/listeProduits.php");
        break;
    case "formAjout":
        $autorisation = $_SESSION["autorisation"] ?? false;
        if ($autorisation) {
            include("vues/formAjout.php");
        }
        break;
    case "traitementAjout":
        $autorisation = $_SESSION["autorisation"] ?? false;
        $tokenForm = securiser($_POST["tokenForm"]);

        if ($autorisation && $tokenForm == $token) {

            $unProduit = new Produit();
            $nom = securiser($_POST["nomProduit"]);
            $prix = securiser($_POST["prixProduit"]);
            $photo = $_FILES["img"]["name"];
            $idCateg = securiser($_POST["idCat"]);
            $unProduit->hydrate($nom, $prix, $photo);
            $unProduit->setIdCat($idCateg);
            $resultat = Produit::ajouter($unProduit);
            var_dump($resultat);
            $nom_image = basename($_FILES["img"]["name"]);
            $chemin = "Images/" . $nom_image;
            move_uploaded_file($_FILES['img']["tmp_name"], $chemin);
            header("location:index.php?uc=admin&action=espace");
        } else {
            header("location:index.php");
        }
        break;
    case "formModif":

        $autorisation = $_SESSION["autorisation"] ?? false;
        if ($autorisation) {
            $idProduit = securiser($_GET["id"]);
            echo $idProduit;
            $leProduit = Produit::afficherParId($idProduit);
            $lesCategories = Categorie::afficherTous();
            var_dump($leProduit);
            include("vues/formModif.php");
        } else {
            //page 404
        }
        break;
    case "traitementModif":
        $autorisation = $_SESSION["autorisation"] ?? false;

        $postSecu = [];
        foreach ($_POST as $index => $uneValeur) {
            $postSecu[$index] = securiser($uneValeur);
        }

        if ($autorisation && $token == $postSecu["tokenForm"]) {
            $nouveauProduit = new Produit();
            if ($_FILES["img"]["name"] == "") {
                $photo = Produit::afficherParId($postSecu["id"])->getPhoto();
            } else {
                $photo = $_FILES["img"]["name"];
                $nom_image = basename($_FILES["img"]["name"]);
                $chemin = "Images/" . $nom_image;
                move_uploaded_file($_FILES['img']["tmp_name"], $chemin);
            }
            $nouveauProduit->hydrate($postSecu["nomProduit"], $postSecu["prixProduit"], $photo);
            $nouveauProduit->setId($postSecu["id"]);
            $nouveauProduit->setidCat($postSecu["idCat"]);
            $resultat = Produit::modifier($nouveauProduit);
            header("location:index.php?uc=admin&action=espace");
        }
        break;
    case "traitementSup":
        $autorisation = $_SESSION["autorisation"] ?? false;
        if ($autorisation &&  $token == securiser($_POST["tokenForm"])) {
            $id = securiser($_POST["id"]);
            $resultat = Produit::delete($id);
            header("location:index.php?uc=admin&action=espace");
        }
        break;

    case "ajoutPanier":
        $id = securiser($_GET["id"]);
        if (!isset($_SESSION["panier"])) {
            $_SESSION["panier"] = [];
        }
        Produit::ajoutPanier($id);
        break;
}
