<?php ob_start(); ?>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col">
                <h1 class="text-center text-danger mt-3"> Bienvenue sur la boutique de bonbons </h1>
            </div>
        </div>
        <div class="col text-center mt-5">
            <a href="index.php"><img src='Images/accueil.png' class="rounded-circle"></a>
        </div>
    </div>
</div>
<?php $content = ob_get_clean();
require("vues/template.php");
?>