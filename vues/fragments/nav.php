<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <a class="navbar-brand"><i class="fas fa-candy-cane fa-4x" style="color:#FF5733;"></i></a>
    <li class="nav-item">
        <a class="nav-link" href="index.php?uc=bonbons&action=liste">Voir les bonbons</a>
    </li>
    <?php if (isset($_SESSION["autorisation"]) && $_SESSION["autorisation"] == "OK") { ?>
        <li class="nav-item">
            <a class="nav-link" href="index.php?uc=admin&action=deco">Logout</a>
        </li>
    <?php } else { ?>

        <li class="nav-item">
            <a class="nav-link" href="index.php?uc=admin&action=formulaire">Login</a>
        </li>
    <?php } ?>
    <li class="nav-item">
        <a class="nav-link btn btn-primary" href="index.php?uc=panier">Panier <span class="badge badge-secondary"><?= isset($_SESSION["panier"]) ? array_sum($_SESSION["panier"]) : 0 ?></span></a>
    </li>
    <form class="form-inline my-2 my-lg-0" method="POST" action="index.php?uc=bonbons&action=recherche">
        <input class=" form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search" name="recherche">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
    </form>
    <ul class="navbar-nav mr-auto">

        <li class="nav-item">
            <a class="nav-link" href=""><i class="fas fa-shopping-cart fa-2x" style="color:#FF5733;"></i></a>
        </li>
    </ul>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">


            <li class="nav-item">
                <a class="nav-link" href="index.php?uc=admin&action=espace">espace admin</a>
            </li>

        </ul>
    </div>
</nav>