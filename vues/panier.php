<?php
ob_start();
?>
<div class="container">
    <h1>Panier</h1>
    <?php if ($panier == false) {
    ?>
        <div class="alert alert-danger">
            Votre panier est vide
        </div>
    <?php } else { ?>
        <table class="table">
            <thead>
                <tr>
                    <th>Produit</th>
                    <th>Qte</th>
                    <th>Prix</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($panier as $id => $qte) {
                ?>
                    <tr>
                        <td><?= $produits[$id]->getNom() ?></td>
                        <td><?= $qte ?></td>
                        <td><?= $produits[$id]->getPrix() ?></td>
                        <td><a class="btn btn-success" href="">+</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>
</div>

<?php
$content = ob_get_clean();
require("template.php");
