<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title> Découverte PHP</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <a class="navbar-brand"><i class="fas fa-candy-cane fa-4x" style="color:#FF5733;"></i></a>
    <li class="nav-item">
      <a class="nav-link" href="index.php?uc=bonbons&action=liste">Voir les bonbons</a>
    </li>
    <?php if (isset($_SESSION["autorisation"]) && $_SESSION["autorisation"] == "OK") { ?>
      <li class="nav-item">
        <a class="nav-link" href="index.php?uc=admin&action=deco">Logout</a>
      </li>
    <?php } else { ?>

      <li class="nav-item">
        <a class="nav-link" href="index.php?uc=admin&action=formulaire">Login</a>
      </li>
    <?php } ?>
    <form class="form-inline my-2 my-lg-0" method="POST" action="index.php?uc=bonbons&action=recherche">
      <input class=" form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search" name="recherche">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
    </form>
    <ul class="navbar-nav mr-auto">

      <li class="nav-item">
        <a class="nav-link" href=""><i class="fas fa-shopping-cart fa-2x" style="color:#FF5733;"></i></a>
      </li>
    </ul>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">


        <li class="nav-item">
          <a class="nav-link" href="index.php?uc=admin&action=espace">espace admin</a>
        </li>

      </ul>
    </div>
  </nav>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>