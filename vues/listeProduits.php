<?php
ob_start();
?>
<div class="row">
    <div class="col-md-4">
        <h2> Nos produits </h2>
    </div>
    <div class="col-md-5">


    </div>
    <div class="col-md-1"> </div>
</div>
<div class="row">
    <table class="table-bordered">
        <?php

        //affichage des resultats 

        foreach ($lesProduits as $produit) {

            echo "<div class='card text-center' style='width: 15rem;'>
							<img class='card-img-top' src='Images/" . $produit->getPhoto() . "' >
							<div class='card-body'>
								<h5 class='card-title'>" . $produit->getNom() . "</h5>
								<p class='card-text'> " . $produit->getPrix() . " €</p>
								<a id='mywish' href='index.php?uc=bonbons&action=ajoutPanier&id=" . $produit->getId() . "' class='btn btn-danger'>Ajouter au <i class='fas fa-cart-plus'></i></a>
							</div>
						</div>";
        }

        $content = ob_get_clean();
        require("template.php");
