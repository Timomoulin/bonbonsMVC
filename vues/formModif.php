<?php ob_start() ?>
<div class="container col-10 col-sm-8 col-md-6 col-lg-4 mx-auto">
    <form class="" action="index.php?uc=bonbons&action=traitementModif" method="POST" enctype="multipart/form-data">
        <h2>Formulaire de modification</h2>
        <input type="hidden" value="<?= $token ?>" name="formToken">
        <input type="hidden" name="id" value="<?= $leProduit->getId() ?>">
        <div class="row">
            <label for="">Nom</label>
            <input class="form-control" required type="text" name="nomProduit" value="<?php echo $leProduit->getNom() ?>">
        </div>

        <div class="row">
            <label for="">Prix</label>
            <input class="form-control" required type="text" name="prixProduit" value="<?php echo $leProduit->getPrix() ?>">
        </div>


        <div class="row">
            <label for="">idCateg</label>
            <select name="idCat" id="">
                <?php foreach ($lesCategories as $uneCat) { ?>
                    <option <?= $uneCat->getId() == $leProduit->getidCat() ? "selected" : "" ?> value="<?= $uneCat->getId() ?>"><?= $uneCat->getLibelle() ?></option>
                <?php } ?>
            </select>

        </div>

        <div class="row">
            <label for="Image"><?= "Précédente image : "  . $leProduit->getPhoto() ?></label>
            <input type="file" class="ml-2" id="Image" name="img">
        </div>






        <div class="row-fluid d-flex justify-content-center align-items-center my-4 mx-4">
            <input type="submit" class="btn btn-danger" name=" valider" value="valider">
        </div>


    </form>
</div>
<?php
$content = ob_get_clean();
require("template.php");
?>