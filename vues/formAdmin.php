<?php
ob_start();
?>
<div class="container">
    <div class="row col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
        <h1>Formulaire Admin</h1>
        <form action="index.php?uc=admin&action=verif" method="post">
            <?=
            isset($_SESSION["error"]) ? '<div class="alert alert-danger">' . $_SESSION["error"] . '</div>' : "" ?>





            <div class="row mb-2">
                <label for="inputEmail">Login* </label>
                <input name="login" id="inputEmail" type="text" required class="form-control">
            </div>
            <div class="row mb-2">
                <label for="inputMdp">MDP* </label>
                <input name="mdp" id="inputMdp" type="password" required minlength="4" class="form-control">
            </div>
            <button class="btn btn-danger">Envoyer</button>
        </form>
    </div>
</div>
<?php
$content = ob_get_clean();
require("template.php");
