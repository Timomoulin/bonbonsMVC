<?php ob_start(); ?>
<div class="container">
    <h1>Page admin</h1>
    <div class="row">
        <a href="index.php?uc=bonbons&action=formAjout" class="btn btn-success">
            Ajouter un bonbons
        </a>
    </div>
    <div class="row">
        <table class="table">
            <theader>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Prix</th>
                    <th>IdCateg</th>
                    <th>Actions</th>
                </tr>
            </theader>
            <tbody>
                <?php foreach ($lesProduits as $unProduit) { ?>
                    <tr>
                        <td><?= $unProduit->getId() ?></td>
                        <td><?= $unProduit->getNom() ?></td>
                        <td><?= $unProduit->getPrix() ?>€</td>
                        <td><?= $unProduit->getIdCat() ?></td>
                        <td>
                            <a href="index.php?uc=bonbons&action=formModif&id=<?= $unProduit->getId() ?>" class="btn btn-success">Modifier</a>

                            <form action="index.php?uc=bonbons&action=traitementSup" method="post">
                                <input type="hidden" name="id" value="<?= $unProduit->getId() ?>">
                                <input type="hidden" name="tokenForm" value="<?= $token ?>">
                                <button class="btn btn-danger">Suprimer</button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php $content = ob_get_clean();
require("vues/template.php");
?>