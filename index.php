<?php
session_start();
var_dump($_SESSION);
if (empty($_SESSION['token'])) {
    $_SESSION['token'] = bin2hex(random_bytes(32));
}
$token = $_SESSION['token'];

include("modeles/monPdo.php");
include("modeles/Produit.class.php");
include("modeles/Admin.class.php");
include("modeles/Categorie.class.php");

function securiser($donnees)
{
    //Efface les espaces en début et fin de chaine
    $donnees = trim($donnees);
    //Enleve les antislashes pour evité d'ignorer du code
    $donnees = stripslashes($donnees);
    //Empeche l'interpretation de balise HTML
    $donnees = htmlspecialchars($donnees);

    return $donnees;
}


if (empty($_GET["uc"])) {
    $uc = "accueil";
} else {
    $uc = securiser($_GET["uc"]);
}
echo "truc";
switch ($uc) {
    case "accueil":
        include("vues/accueil.php");
        break;
    case "bonbons":
        include("controleurs/controleurBonbons.php");

        break;
    case "panier":
        if (!isset($_SESSION["panier"])) {
            $panier = false;
        } else {
            $panier = $_SESSION["panier"];
            $produits = [];
            foreach ($panier as $id => $qte) {
                $produits[$id] = Produit::afficherParId($id);
            }
        }
        include("vues/panier.php");
        break;
    case "admin":
        include("controleurs/controleurAdmin.php");
        break;
}
