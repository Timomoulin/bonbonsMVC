<?php
class Admin
{
    private string $login;
    private string $mpd;

    public function getLogin()
    {
        return $this->login;
    }

    public function getMdp()
    {
        return $this->mpd;
    }

    public function setLogin($newLogin)
    {
        if (strlen($newLogin) >= 2) {
            $this->login = $newLogin;
        }
    }

    public function setMdp($newMdp)
    {
        if (strlen($newMdp) >= 4) {
            $this->mpd = $newMdp;
        }
    }

    public static function verifier($login, $mdp)
    {
        $mdp = md5($mdp);
        $req = MonPdo::getInstance()->prepare("select * from admin where    login =:login and mdp=:mdp");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'admin');
        $req->bindParam('login', $login);
        $req->bindParam('mdp', $mdp);
        $req->execute();
        $leResultat = $req->fetchAll();
        $nb_lignes = count($leResultat);

        if ($nb_lignes == 0) {
            $rep = false;
        } else {
            $rep = true;
        }

        return $rep;
    }

    public static function deco()
    {
        if (isset($_SESSION["autorisation"])) {
            unset($_SESSION["autorisation"]);
        }
    }
}
