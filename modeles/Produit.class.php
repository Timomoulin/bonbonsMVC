<?php

class Produit
{
    private $id;
    private $nom;
    private $prix;
    private $photo;
    private $idCat;

    public function hydrate($nom, $prix, $photo)
    {
        $this->setNom($nom);
        $this->setPrix($prix);
        $this->setPhoto($photo);
    }

    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setidCat($id)
    {
        $this->idCat = $id;
    }
    public function getidCat()
    {


        return $this->idCat;
    }
    /**
     * Get the value of photo
     */
    public function getPhoto()
    {


        return $this->photo;
    }

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of prix
     */
    public function getPrix(): int
    {
        return $this->prix;
    }

    public static function afficherTous()
    {
        $req = MonPdo::getInstance()->prepare("select * from produit");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats = $req->fetchAll();
        return $lesResultats;
    }

    public static function afficherParId($unId): Produit
    {
        $req = MonPdo::getInstance()->prepare("SELECT * from PRODUIT where id=:id"); //la table produit
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Produit'); // la classe produit
        $req->bindValue(":id", $unId);
        $req->execute();
        $lesResultats = $req->fetch();
        return $lesResultats;
    }

    public static function recherche($unNom)
    {
        $req = MonPdo::getInstance()->prepare("select * from produit where nom like :unNom");
        $req->bindValue(":unNom", "%$unNom%");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats = $req->fetchAll();
        return $lesResultats;
    }

    public static function ajouter(Produit $produit)
    {
        try {
            $req = MonPdo::getInstance()->prepare("insert into produit(nom, prix, photo,idCat) values(:nom, :prix, :photo,:idCat)");
            $req->bindValue(':nom', $produit->getNom());
            $req->bindValue(':prix', $produit->getPrix());
            $req->bindValue(':photo', $produit->getPhoto());
            $req->bindValue(':idCat', $produit->getidCat());
            $nb = $req->execute();
            return $nb;
        } catch (PDOException $e) {
            var_dump($e);
            echo $e->getMessage();
        }
    }

    public static function modifier(Produit $produit)
    {
        try {
            $req = MonPdo::getInstance()->prepare("UPDATE produit set nom=:nom, prix=:prix, idCat=:idCat, photo=:photo where id=:id");
            $req->bindValue(':nom', $produit->getNom());
            $req->bindValue(':prix', $produit->getPrix());
            $req->bindValue(':photo', $produit->getPhoto());
            $req->bindValue(':idCat', $produit->getidCat());
            $req->bindValue(':id', $produit->getId());
            $nb = $req->execute();
            return $nb;
        } catch (PDOException $e) {
            var_dump($e);
            echo $e->getMessage();
            return false;
        }
    }
    public static function delete($idProduit)
    {
        try {

            $req = MonPdo::getInstance()->prepare("DELETE FROM produit where id=:id");
            $req->bindValue(':id', $idProduit);
            $nb = $req->execute();

            return true;
        } catch (PDOException $e) {
            var_dump($e);
            echo $e->getMessage();

            return false;
        }
    }
    public static function ajoutPanier($id)
    {
        $panier = $_SESSION["panier"];
        if (isset($panier[$id])) {
            $_SESSION["panier"][$id]++;
        } else {
            $_SESSION["panier"][$id] = 1;
        }
    }
}
