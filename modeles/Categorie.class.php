<?php
class Categorie
{
    private int $id;
    private string $libelle;



    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of libelle
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @return  self
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    public static function afficherTous()
    {
        $req = MonPdo::getInstance()->prepare("select * from categorie");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'categorie');
        $req->execute();
        $lesResultats = $req->fetchAll();
        return $lesResultats;
    }
}
